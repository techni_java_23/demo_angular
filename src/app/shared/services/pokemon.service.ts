import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PokemonDetail, PokemonResult } from '../models/pokemon';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private _httpClient: HttpClient) { }

  getMany(url: string): Observable<PokemonResult> {

    return this._httpClient.get<PokemonResult>(url);
  }

  getOne(url: string): Observable<PokemonDetail> {

    return this._httpClient.get<PokemonDetail>(url);
  }
}
