export interface PokemonResult {

    count: number;
    next?: string;
    previous?: string;
    results: PokemonSimple[];
}

export interface PokemonSimple {

    name: string;
    url: string;
    front_shiny: string;
}

export interface PokemonDetail {

    id: number;
    name: string;
    height: number;
    weight: number;
    sprites: Sprite;
}

export interface Sprite {
    front_shiny: string;
    back_shiny: string;
}