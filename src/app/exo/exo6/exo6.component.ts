import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PokemonDetail, PokemonResult } from 'src/app/shared/models/pokemon';
import { PokemonService } from 'src/app/shared/services/pokemon.service';

@Component({
  selector: 'app-exo6',
  templateUrl: './exo6.component.html',
  styleUrls: ['./exo6.component.scss']
})
export class Exo6Component implements OnInit {

  setUpList: string[] = new Array<string>(20).fill('----------');
  pokemonResult!: PokemonResult;
  pokemonDetail: PokemonDetail | undefined

  constructor(
    private _pokemonService: PokemonService,
    private _httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    this.getMany('https://pokeapi.co/api/v2/pokemon/');
  }

  getMany(url: string): void {
    this._pokemonService.getMany(url).subscribe({
      next: (result) => {
        this.pokemonResult = result;
        for (let pokemon of this.pokemonResult.results) {
          this._httpClient.get(pokemon.url).subscribe({
            next: (data: any) => {
              pokemon.front_shiny = data.sprites.front_shiny;
            }
          })
        }
      }
    })
  }

  getOne(url: string): void {
    this._pokemonService.getOne(url).subscribe({
      next: (result) => {
        this.pokemonDetail = result;
      }
    })
  }

  next(): void {

    if (this.pokemonResult.next) {

      this.getMany(this.pokemonResult.next);
    }
  }

  prev(): void {

    if (this.pokemonResult.previous) {

      this.getMany(this.pokemonResult.previous);
    }
  }
}
